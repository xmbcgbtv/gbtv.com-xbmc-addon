import urllib
import urllib2
import re
import os
import xbmcplugin
import xbmcgui
import xbmcaddon
import cookielib
import datetime
import time
from BeautifulSoup import BeautifulSoup, BeautifulStoneSoup

try:
    import json
except:
    import simplejson as json

__settings__ = xbmcaddon.Addon(id='plugin.video.gbtv')
home = __settings__.getAddonInfo('path')
icon = xbmc.translatePath( os.path.join( home, 'icon.png' ) )
fanart = xbmc.translatePath( os.path.join( home, 'fanart.jpg' ) )

#
# Show Listing
#
showsmain = ['radio_show','fourth_hour','glenn_beck_program','liberty_treehouse','b_s_of_a','uncensored','mercury_theater']
showslist = ['beck_u','gbtv_insider','making_of_gbtv','documentary','special_program','restore_courage','radio_show','fourth_hour','glenn_beck_program','liberty_treehouse','b_s_of_a','uncensored','mercury_theater']
freeshowslist = ['radio_show','fourth_hour','glenn_beck_program','liberty_treehouse','b_s_of_a','uncensored','beck_u','special_program','restore_courage']

#
# Show Details 
#
showsdetails = {}
showsdetails['radio_show'] = {'name': 'Glenn Beck Radio Show', 'logo': 'http://mptechnology.bitbucket.org/xbmcaddons/images/gbtv.0.0.3/radiologo.jpg', 'description': 'Why just listen to Glenn when you can watch him too! GBTV is your "All Access" pass inside the radio studio. Tune in weekdays from 9am-12p eastern and watch Glenn, Pat and Stu in our fully produced 5 camera video broadcast of the radio program. It\'s the only place to see all the behind the scenes action where Glenn is on and off the air. '}
showsdetails['fourth_hour']= {'name': 'The 4th Hour with Stu & Pat', 'logo': 'http://mptechnology.bitbucket.org/xbmcaddons/images/gbtv.0.0.3/4thHour.jpg', 'description': 'Immediately following the Glenn Beck Radio Program, the show continues on The 4th Hour with Stu and Pat. It\'s an extra hour produced exclusively for GBTV subscribers only. This bonus hour is dedicated to covering the topics Glenn didn\'t get to, taking your phone calls, sharing interesting facts about Former President James K. Polk and much, much more. It\'s The 4th Hour - Weekdays from 12p - 1p eastern. ' }
showsdetails['glenn_beck_program'] = {'name': 'Glenn Beck (tv show)', 'logo': 'http://mptechnology.bitbucket.org/xbmcaddons/images/gbtv.0.0.3/gbtv.jpeg', 'description' : 'Everything you know and love about Glenn just got bigger and better! The Glenn Beck Show on GBTV is now two hours long and the only place to see him in prime time. Watch it weekdays from 5-7pm ET starting September 12th. '}
showsdetails['liberty_treehouse']=  {'name': 'Liberty Treehouse', 'logo': 'http://mptechnology.bitbucket.org/xbmcaddons/images/gbtv.0.0.3/liberty.jpg', 'description' : 'The whys and wherefores of the day\'s news - the things the grownups are talking about - and a look at the places where the news is unfolding. History, Science, Video Games, and the Arts, sliced, diced, and discussed like no other show on TV or anywhere else. All that, plus a look at classic episodes of the TV shows that mom and dad, maybe even grandpa and grandma considered "must see." If you have a question for host Raj Nair, email him directly at askraj@gbtv.com. '}
showsdetails['b_s_of_a'] =  {'name': 'The B.S. of A. with Brian Sack', 'logo': 'http://mptechnology.bitbucket.org/xbmcaddons/images/gbtv.0.0.3/bsa.jpg', 'description' : 'GBTV\'s chief humorist Brian Sack has been given a mandate: To stay objective, rational and witty as he views the news of the day through a nonpartisan lens. He\'ll favor no politics, no politician and no party as he delivers a daily dose of...The B.S. of A. ' }
showsdetails['uncensored'] =  {'name': 'Uncensored', 'logo': 'http://mptechnology.bitbucket.org/xbmcaddons/images/gbtv.0.0.3/uncensored.jpg', 'description' : 'S.E. Cupps harding hitting political interviews '}
showsdetails['beck_u'] =  {'name': 'Beck University', 'logo': 'http://mptechnology.bitbucket.org/xbmcaddons/images/gbtv.0.0.3/becku.jpg', 'description': 'Beck University is a unique academic experience bringing together experts in the fields of religion, American history and economics. We can no longer rely on our text books that have been hijacked by progressives. Learn history the way it really happened at Beck University.' }
showsdetails['mercury_theater'] =  {'name': 'Mercury Theater', 'logo': 'http://mptechnology.bitbucket.org/xbmcaddons/images/gbtv.0.0.3/mercurytheater.jpg', 'description': 'Hosted by Brian Sack, this one hour program at 1pm ET Monday through Friday will introduce, or reintroduce, GBTV viewers to the shows and stars that made television the cultural icon it is today. Mercury Theater will feature classic episodes from the likes of Milton Berle and Lucille Ball and one of a kind series like Flash Gordon and Bonanza. '}
showsdetails['gbtv_insider'] =  {'name': 'GBTV Insider', 'logo': 'http://mptechnology.bitbucket.org/xbmcaddons/images/gbtv.0.0.3/insider.jpg', 'description': 'Behind the scenes looks at GBTV' }
showsdetails['making_of_gbtv'] =  {'name': 'Making Of GBTV', 'logo': 'http://mptechnology.bitbucket.org/xbmcaddons/images/gbtv.0.0.3/makingof.jpg', 'description': 'The making of GBTV is the first reality series produced by Mercury Radio Arts. Not only will you get a glimpse of what to expect from GBTV, but we\'ll also take you through the step by step process of creating a brand new media network. Make sure you tune in every Thursday to see the latest developments, starting Thursday June 23rd.' }
showsdetails['documentary'] =  {'name': 'GBTV: Documentaries', 'logo': 'http://mptechnology.bitbucket.org/xbmcaddons/images/gbtv.0.0.3/docu.jpg', 'description' : 'Hard hitting documentaries made just for GBTV' }
showsdetails['special_program'] =  {'name': 'GBTV: Specials', 'logo': 'http://mptechnology.bitbucket.org/xbmcaddons/images/gbtv.0.0.3/specials.jpg', 'description' : 'Special Events and Special programs just for GBTV Subscribers'}
showsdetails['restore_courage'] =  {'name': 'Restoring Courage: Stand with Israel', 'logo': 'http://mptechnology.bitbucket.org/xbmcaddons/images/gbtv.0.0.3/courage.jpg', 'description' : 'If you missed the Restoring Courage Rally that took place on 8/24/2011 now is your chance to watch it, or rewatch it' }

SOAPCODES = {
    "1"    : "OK",
    "-1000": "Requested Media Not Found",
    "-1500": "Other Undocumented Error",
    "-2000": "Authentication Error",
    "-2500": "Blackout Error",
    "-3000": "Identity Error",
    "-3500": "Sign-on Restriction Error",
    "-4000": "System Error",
    }

def categories():
	if __settings__.getSetting('prem_content') == 'true':	
		addDir("Today's Live Shows",'http://web.gbtv.com/gen/hb/video/'+str(datetime.date.today()).replace('-','/')+'/grid.json',4,icon)
		addDir("Today's Archived Shows",'today',90,icon)
	
		for show in showsmain:
			 addDir(showsdetails[show]['name'],show,70,showsdetails[show]['logo'], showsdetails[show]['description'])
		addDir("More Shows, Specials, & Documentaries",'more',91,icon)
	else:
		for show in freeshowslist:
			addDir(showsdetails[show]['name'],show,70,showsdetails[show]['logo'], showsdetails[show]['description'])

def getAllShows():
	if __settings__.getSetting('prem_content') == 'true':	
		for show in showslist:
			 addDir(showsdetails[show]['name'],show,70,showsdetails[show]['logo'], showsdetails[show]['description'])
	else:
		for show in freeshowslist:
			addDir(showsdetails[show]['name'],show,70,showsdetails[show]['logo'], showsdetails[show]['description'])
		
def getEpisodes(show=""):
   	if __settings__.getSetting('prem_content') == 'true':

		if show in freeshowslist:
			addDir(showsdetails[show]['name']+' - Highlights / Clips - Free Content',show,80,icon)
		index('http://web.gbtv.com/ws/search/MediaSearchService?subject=GBTV_LIVE&query='+show+'&sort=desc&sort_type=date&hitsPerPage=5&src=vpp',0)
		addDir('Full Episode List for: '+showsdetails[show]['name'],show,75,showsdetails[show]['logo'], showsdetails[show]['description'])
	else:
		index('http://web.gbtv.com/ws/search/MediaSearchService?query='+show+'&sort=desc&sort_type=date&hitsPerPage=200&src=vpp',1,0)		

def getAllEpisodes(show):
   	if __settings__.getSetting('prem_content') == 'true':
		details = showsdetails[show]
		if show in freeshowslist:
			addDir(showsdetails[show]['name']+' - Highlights / Clips - Free Content',show,80,showsdetails[show]['logo'], showsdetails[show]['description'])
		index('http://web.gbtv.com/ws/search/MediaSearchService?subject=GBTV_LIVE&query='+show+'&sort=desc&sort_type=date&hitsPerPage=200&src=vpp',0)
	else:
		index('http://web.gbtv.com/ws/search/MediaSearchService?query='+show+'&sort=desc&sort_type=date&hitsPerPage=200&src=vpp',1,0)		

def getTodaysShows():
   	if __settings__.getSetting('prem_content') == 'true':
		index('http://web.gbtv.com/ws/search/MediaSearchService?subject=GBTV_LIVE&query='+str(datetime.date.today())+'&sort=asc&sort_type=date&hitsPerPage=200&src=vpp',0)
	else:
		index('http://web.gbtv.com/ws/search/MediaSearchService?query='+show+'&sort=asc&sort_type=date&hitsPerPage=200&src=vpp',1,0)		


def getHighlights(show=""):
	index('http://web.gbtv.com/ws/search/MediaSearchService?query='+str(datetime.date.today())+'&sort=desc&sort_type=date&hitsPerPage=200&src=vpp',1,0)

def index(url,showfree=1,showprem=1):
        headers = {'User-agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:6.0) Gecko/20100101 Firefox/6.0',
                   'Referer' : 'http://web.gbtv.com/media/video.jsp'}
        req = urllib2.Request(url,None,headers)
       
        response = urllib2.urlopen(req)
        print "JSON Retrieved Prosessing- "+str(datetime.datetime.today())
        link=response.read()
        response.close()
        data = json.loads(link)
        print "JSON Processed adding links- "+str(datetime.datetime.today())
        items = data['mediaContent']

        for i in items:
            name = i['blurb']
            url = i['url']
            desc = i['bigBlurb']
            try:
                duration = i['duration']
            except:
                duration = ''
            date = i['date_added']
            try:
                thumb = i['thumbnails'][0]['src']
                if thumb.startswith('/'):
                    thumb = 'http://web.gbtv.com'+thumb
            except:
                thumb = ''
            contentId = i['contentId']
            for event in i['keywords']:
                if event['type'] == 'calendar_event_id':
                    eventId = event['keyword']
            description = desc+' \n'+date
            if not duration == '' and showfree == 1:
                addLink(name,url,description,duration,thumb,2)
            elif duration == '' and showprem == 1:
                if __settings__.getSetting('prem_content') == 'true':
                    playbackScenario = __settings__.getSetting('archive_scenario')
                    addPremLink(name+' | Episode',url,eventId,contentId,playbackScenario,description,thumb,3)
        
      
def get_video(url):
        headers = {'User-agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:6.0) Gecko/20100101 Firefox/6.0',
                   'Referer' : 'http://web.gbtv.com/media/video.jsp'}
        req = urllib2.Request(url,None,headers)
        response = urllib2.urlopen(req)
        link=response.read()
        soup = BeautifulStoneSoup(link, convertEntities=BeautifulStoneSoup.XML_ENTITIES)
        if __settings__.getSetting('scenario') == 'FLASH_1200K_640X360':
            try:
                url = soup.find('url', attrs={'playback_scenario' : "FLASH_1200K_640X360"}).string
            except:
                print '######## Playback scenario not found. #########'
                return
            item = xbmcgui.ListItem(path=url)
            xbmcplugin.setResolvedUrl(int(sys.argv[1]), True, item)
        elif  __settings__.getSetting('scenario') == 'FLASH_600K_400X224':
            try:
                url = soup.find('url', attrs={'playback_scenario' : "FLASH_600K_400X224"}).string
            except:
                print '######## Playback scenario not found. #########'
                return
            item = xbmcgui.ListItem(path=url)
            xbmcplugin.setResolvedUrl(int(sys.argv[1]), True, item)


def get_live_video(url):
        txheaders = {'User-agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:6.0) Gecko/20100101 Firefox/6.0',
                     'Referer' : 'http://web.gbtv.com/schedule/index.jsp'}
        req = urllib2.Request(url,None,txheaders)
        response = urllib2.urlopen(req)
        link = response.read()
        data = json.loads(link)
        items = data['shows']['show']
        if len(items) < 1:
            xbmc.executebuiltin("XBMC.Notification(GBTV,No Live Shows Found,10000,"+icon+")")
            return
        for i in items:
            state = i['media_state']
            desc = i['show_media']['homebase']['media']['bigblurb']
            name = i['show_media']['homebase']['media']['header']
            thumb = 'http://web.gbtv.com'+i['show_media']['homebase']['media']['thumbnails']['thumbnail'][0]['url']
            eventId = i['calendar_event_id']
            contentId = i['show_media']['homebase']['media']['id']
            start = i['local_start_time']
            if state == 'MEDIA_ON':
                playbackScenario = __settings__.getSetting('live_scenario')
                addPremLink(name+' | Live Now','url',eventId,contentId,playbackScenario,desc,thumb,3)
            elif state == 'MEDIA_ARCHIVE':
                name = name+' | Archived'
                liz=xbmcgui.ListItem(name, iconImage="DefaultVideo.png", thumbnailImage=thumb)
                liz.setInfo( type="Video", infoLabels={ "Title": name, "Plot":desc } )
                liz.setProperty( "Fanart_Image", fanart )
                ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url='',listitem=liz)
            else:
                name = name+' | Media off'
                liz=xbmcgui.ListItem(name, iconImage="DefaultVideo.png", thumbnailImage=thumb)
                liz.setInfo( type="Video", infoLabels={ "Title": name, "Plot":desc } )
                liz.setProperty( "Fanart_Image", fanart )
                ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url='',listitem=liz)


def get_prem_video(eventId, contentId, playbackScenario):
        xbmc.executebuiltin("XBMC.Notification(GBTV, Premium Content - Logging In, 5000, "+icon+")")
        cj = cookielib.LWPCookieJar()
        opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
        urllib2.install_opener(opener)
        # Get the cookie first
        theurl = 'https://secure.gbtv.com/enterworkflow.do?flowId=registration.migrate&forwardUrl=/account/home.jsp&platform=WEB'
        txheaders = {'User-agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:6.0) Gecko/20100101 Firefox/6.0',
                     'Referer' : 'http://web.gbtv.com/media/video.jsp'}
        req = urllib2.Request(theurl,None,txheaders)
        response = urllib2.urlopen(req)

        # Now login
        #theurl = 'https://secure.gbtv.com/authenticate.do'
        theurl = 'https://secure.gbtv.com/enterworkflow.do?flowId=subscriptions.updatesubscription'
        txheaders = {'User-agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:6.0) Gecko/20100101 Firefox/6.0',
                     'Referer' : 'https://secure.gbtv.com/enterworkflow.do?flowId=registration.migrate&forwardUrl=/account/home.jsp&platform=WEB'}
        values = {'uri' : '/account/login_register.jsp',
                  'registrationAction' : 'identify',
                  'emailAddress' : __settings__.getSetting('email'),
                  'password' : __settings__.getSetting('password'),
                  'submit' : 'Login'}

        req = urllib2.Request(theurl,urllib.urlencode(values),txheaders)
        response = urllib2.urlopen(req)

        # print response.info()
        # print 'These are the cookies we have received so far :
        # for index, cookie in enumerate(cj):
            # print index, '  :  ',cookie

        # Begin MORSEL extraction
        ns_headers = response.headers.getheaders("Set-Cookie")
        attrs_set = cookielib.parse_ns_headers(ns_headers)
        cookie_tuples = cookielib.CookieJar()._normalized_cookie_tuples(attrs_set)
        cookies = {}
        for tup in cookie_tuples:
            name, value, standard, rest = tup
            cookies[name] = value

        page = response.read()
        soup = BeautifulSoup(page)

        if soup.title.string == 'GBTV.com: Account: Account Management - My Subscriptions':
            print "Logged in successfully!"
            # xbmc.executebuiltin("XBMC.Notification(GBTV, Logged in successfully!, 5000, "+icon+")")
        else:
            print 'Login Failed'
            print page
            xbmc.executebuiltin("XBMC.Notification(GBTV, Login Failed, 5000, "+icon+")")
            return

        values = {
            'eventId': eventId,
            'fingerprint': urllib.unquote(cookies['fprt']),
            'identityPointId': cookies['ipid'],
            'subject':'GBTV_LIVE',
            'platform':'WEB_MEDIAPLAYER'
        }

        theurl = 'https://secure.gbtv.com/pubajaxws/bamrest/MediaService2_0/op-findUserVerifiedEvent/v-2.3?'
        headers = {'User-agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:6.0) Gecko/20100101 Firefox/6.0',
                   'Referer' : 'http://web.gbtv.com/account/home.jsp'}
        req = urllib2.Request(theurl,urllib.urlencode(values),headers)
        response = urllib2.urlopen(req)
        link=response.read()
        soup = BeautifulStoneSoup(link, convertEntities=BeautifulStoneSoup.XML_ENTITIES)
        eventId = soup('event-id')[0].string
        contentId = soup('content-id')[0].string
        sessionKey = soup('session-key')[0].string
        status_code = soup('status-code')[0].string
        if status_code != "1":
            try:
                error_str = SOAPCODES[status_code]
                print 'SOAPCODE : '+error_str
            except:
                pass
            try:
                status_message = soup('status-message')[0].string
                print 'Status Message : '+status_message
                xbmc.executebuiltin("XBMC.Notification(GBTV,"+status_message+",10000,"+icon+")")
            except:
                xbmc.executebuiltin("XBMC.Notification(GBTV,Unknown Error - Check Log,10000,"+icon+")")
            print link
            return

        values = {
            'contentId': contentId,
            'eventId': eventId,
            'sessionKey': sessionKey,
            'fingerprint': urllib.unquote(cookies['fprt']),
            'identityPointId': cookies['ipid'],
            'subject':'GBTV_LIVE',
            'playbackScenario': playbackScenario,
        }

        theurl = 'https://secure.gbtv.com/pubajaxws/bamrest/MediaService2_0/op-findUserVerifiedEvent/v-2.3?'
        headers = {'User-agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:6.0) Gecko/20100101 Firefox/6.0',
                   'Referer' : 'http://web.gbtv.com/account/home.jsp'}
        req = urllib2.Request(theurl,urllib.urlencode(values),headers)
        response = urllib2.urlopen(req)
        link=response.read()
        soup = BeautifulStoneSoup(link, convertEntities=BeautifulStoneSoup.XML_ENTITIES)
        try:
            rtmp = soup.url.string
            print 'RTMP : '+rtmp
        except:
            xbmc.executebuiltin("XBMC.Notification(GBTV, "+soup('status-message')[0].string+", 10000, "+icon+")")
            print link
            return
        if 'ondemand/' in rtmp:
            Playpath = ' Playpath='+rtmp.split('ondemand/')[1]
            swf = ' swfUrl=http://web.gbtv.com/shared/flash/mediaplayer/v4.2/R12/MediaPlayer4.swf?v=14'
        elif 'live/' in rtmp:
            Playpath = ' Playpath='+rtmp.split('live/')[1]
            swf = ' swfUrl=http://web.gbtv.com/shared/flash/mediaplayer/v4.2/R12/MediaPlayer4.swf?v=14 live=1'
        Pageurl = ' pageUrl=http://web.gbtv.com/shared/flash/mediaplayer/v4.2/R12/MP4.jsp?calendar_event_id='+eventId+'&content_id='+contentId+'&media_id=&view_key=&media_type=video&source=GBTV&sponsor=GBTV&clickOrigin=&affiliateId='
        url = rtmp+Playpath+Pageurl+swf
        item = xbmcgui.ListItem(path=url)

         # Logout
        theurl = 'https://secure.gbtv.com/enterworkflow.do?flowId=registration.logout'
        headers = {'User-agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:6.0) Gecko/20100101 Firefox/6.0',
                   'Referer' : 'http://web.gbtv.com/account/home.jsp'}
        req = urllib2.Request(theurl,None,headers)
        response = urllib2.urlopen(req)
        link=response.read()
        soup = BeautifulStoneSoup(link, convertEntities=BeautifulStoneSoup.XML_ENTITIES)
        print soup.title.string

        xbmcplugin.setResolvedUrl(int(sys.argv[1]), True, item)


def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
            params=sys.argv[2]
            cleanedparams=params.replace('?','')
            if (params[len(params)-1]=='/'):
                params=params[0:len(params)-2]
            pairsofparams=cleanedparams.split('&')
            param={}
            for i in range(len(pairsofparams)):
                splitparams={}
                splitparams=pairsofparams[i].split('=')
                if (len(splitparams))==2:
                    param[splitparams[0]]=splitparams[1]
        return param


def addLink(name,url,description,duration,iconimage,mode):
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)
        ok=True
        liz=xbmcgui.ListItem(name, iconImage="DefaultVideo.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name, "Plot":description, "Duration":duration } )
        liz.setProperty( "Fanart_Image", fanart )
        liz.setProperty('IsPlayable', 'true')
        ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz)
        return ok


def addPremLink(name,url,eventId,contentId,playbackScenario,description,iconimage,mode):
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)+"&contentId="+str(contentId)+"&eventId="+str(eventId)+"&playbackScenario="+str(playbackScenario)
        ok=True
        liz=xbmcgui.ListItem(name, iconImage="DefaultVideo.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name, "Plot":description } )
        liz.setProperty( "Fanart_Image", fanart )
        liz.setProperty('IsPlayable', 'true')
        ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz)
        return ok


def addDir(name,url,mode,iconimage,description = ''):
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)
        ok=True
        liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name , "Plot":description} )
        liz.setProperty( "Fanart_Image", fanart )
        ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
        return ok


params=get_params()
url=None
name=None
mode=None

try:
    url=urllib.unquote_plus(params["url"])
except:
    pass
try:
    name=urllib.unquote_plus(params["name"])
except:
    pass
try:
    mode=int(params["mode"])
except:
    pass
try:
    contentId=str(params["contentId"])
except:
    pass
try:
    eventId=str(params["eventId"])
except:
    pass
try:
    playbackScenario=str(params["playbackScenario"])
except:
    pass

print "Mode: "+str(mode)
print "URL: "+str(url)
print "Name: "+str(name)

if mode==None or url==None or len(url)<1:
    print ""
    categories()

elif mode==1:
    print ""
    index(url)

elif mode==2:
    print ""
    get_video(url)

elif mode==3:
    print ""
    get_prem_video(eventId, contentId, playbackScenario)

elif mode==4:
    print ""
    get_live_video(url)
elif mode == 70:
	getEpisodes(url)
elif mode == 75:
	getAllEpisodes(url)
elif mode == 80:
	getHighlights(url)
elif mode == 90:
	getTodaysShows()
elif mode == 91:
	getAllShows()
	
print "Passed back to XBMC "+str(datetime.datetime.today())
xbmcplugin.endOfDirectory(int(sys.argv[1]))
